from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import LanguageViewSet

router = SimpleRouter()
router.register(r"", LanguageViewSet, basename="languages")

urlpatterns = [path("", include(router.urls))]
