from rest_framework import serializers

from .models import Language


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ("id", "name", "versions")

    def create(self, validated_data):
        # Ensure languages are always uppercased or titlecased
        name: str = validated_data.get("name")
        if not name.isupper():
            name = name.title()
        language = Language.objects.create(name=name)
        return language

    def update(self, instance, validated_data):
        # Ensure languages are always uppercased or titlecased
        name = validated_data.get("name")
        if not name.isupper():
            name = name.title()
        instance.name = name
        instance.save()
        return instance
