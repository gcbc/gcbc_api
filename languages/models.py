from django.db import models


class Language(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False, unique=True)

    class Meta:
        db_table = "language"
        ordering = ["name"]

    def __str__(self):
        return self.name
