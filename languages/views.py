from rest_framework import viewsets

from gcbc_api.permissions import IsAdminOrReadOnly

from .models import Language
from .serializers import LanguageSerializer


class LanguageViewSet(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer

    permission_classes = [IsAdminOrReadOnly]
