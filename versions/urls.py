from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import VersionViewSet

router = SimpleRouter()
router.register(r"", VersionViewSet, basename="versions")

urlpatterns = [path("", include(router.urls))]
