from django.db import models

from languages.models import Language


class Version(models.Model):
    value = models.CharField(max_length=20, unique=True)
    languages = models.ManyToManyField(Language, related_name="versions")

    class Meta:
        db_table = "version"

    def __str__(self):
        return self.value
