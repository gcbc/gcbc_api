from rest_framework import viewsets

from gcbc_api.permissions import IsAdminOrReadOnly

from .models import Version
from .serializers import VersionSerializer


class VersionViewSet(viewsets.ModelViewSet):
    queryset = Version.objects.all()
    serializer_class = VersionSerializer

    permission_classes = [IsAdminOrReadOnly]
