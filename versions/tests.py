import pytest
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from versions.models import Version


@pytest.mark.django_db
class TestVersions(APITestCase):
    super_username = "picard"
    super_email = "picard@example.com"
    super_password = "makeitso"
    api = "/api/v1"

    def setUp(self):
        user_model = get_user_model()
        self.superuser = user_model.objects.create_superuser(
            username=self.super_username,
            email=self.super_email,
            password=self.super_password,
        )

        self.regular_user = user_model.objects.get(username="testregularuser")
        self.regular_user.set_password("abc123")
        self.regular_user.save()

    def test_versions_unauthenticated(self):
        url = f"{self.api}/versions/"
        response = self.client.get(url)
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
            or status.HTTP_403_FORBIDDEN
        )

    def test_versions(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/versions/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 2

    def test_version_create(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/versions/"
        response = self.client.post(url, {"id": 3, "value": "3.7.1", "languages": [1]})
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.data) == 3
        assert response.data == {"id": 3, "value": "3.7.1", "languages": [1]}

    def test_versions_retrieve(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/versions/1/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_versions_update(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/versions/1/"
        response = self.client.put(url, {"id": 1, "value": "3.7.4", "languages": [1]})
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_version_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/versions/2/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/versions/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 1

    def test_version_delete_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/versions/1/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

        url = f"{self.api}/versions/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 2

    def test_version_str_representation(self):
        version = Version.objects.get(pk=1)
        assert str(version) == "3.6.5"
