from django.contrib import admin

from .models import Complexity

admin.site.register(Complexity)
