from django.apps import AppConfig


class ComplexitiesConfig(AppConfig):
    name = "complexities"
