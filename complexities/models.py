from django.db import models


class Complexity(models.Model):
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=150)

    class Meta:
        db_table = "complexity"

    def __str__(self):
        return self.name
