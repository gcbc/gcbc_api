from rest_framework import serializers

from .models import Complexity


class ComplexitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Complexity
        fields = ("id", "name", "description")
