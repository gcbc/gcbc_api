from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import ComplexityViewSet

router = SimpleRouter()
router.register(r"", ComplexityViewSet, basename="complexities")

urlpatterns = [path("", include(router.urls))]
