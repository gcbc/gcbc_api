from rest_framework import viewsets

from gcbc_api.permissions import IsAdminOrReadOnly

from .models import Complexity
from .serializers import ComplexitySerializer


class ComplexityViewSet(viewsets.ModelViewSet):
    queryset = Complexity.objects.all()
    serializer_class = ComplexitySerializer

    permission_classes = [IsAdminOrReadOnly]
