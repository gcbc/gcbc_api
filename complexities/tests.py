import pytest
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from complexities.models import Complexity


@pytest.mark.django_db
class TestComplexities(APITestCase):
    super_username = "picard"
    super_email = "picard@example.com"
    super_password = "makeitso"
    api = "/api/v1"

    def setUp(self):
        user_model = get_user_model()
        self.superuser = user_model.objects.create_superuser(
            username=self.super_username,
            email=self.super_email,
            password=self.super_password,
        )

        self.regular_user = user_model.objects.get(username="testregularuser")
        self.regular_user.set_password("abc123")
        self.regular_user.save()

    def test_complexity_unauthenticated(self):
        url = f"{self.api}/complexities/"
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 7

    def test_complexity_create(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/complexities/"
        response = self.client.post(
            url, {"name": "test name", "description": "test_description"}
        )
        self.client.logout()
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data == {
            "id": 8,
            "name": "test name",
            "description": "test_description",
        }

    def test_complexity_create_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/complexities/"
        response = self.client.post(
            url, {"name": "test name", "description": "test_description"}
        )
        self.client.logout()
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_complexity_retrieve(self):
        url = f"{self.api}/complexities/1/"
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {
            "id": 1,
            "name": "O(1)",
            "description": "Constant time",
        }

    def test_complexity_update(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/complexities/1/"
        response = self.client.put(
            url, {"id": 1, "name": "test", "description": "test"}
        )
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert response.data == {"id": 1, "name": "test", "description": "test"}

    def test_complexity_update_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/complexities/1/"
        response = self.client.post(
            url, {"id": 1, "name": "test name", "description": "test_description"}
        )
        self.client.logout()
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_complexity_update_anonymous(self):
        url = f"{self.api}/complexities/1/"
        response = self.client.post(
            url, {"id": 1, "name": "test name", "description": "test_description"}
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_complexity_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/complexities/2/"
        response = self.client.delete(url)
        self.client.logout()
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/complexities/"
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 6

    def test_complexity_delete_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/complexities/1/"
        response = self.client.delete(url)
        self.client.logout()
        assert response.status_code == status.HTTP_403_FORBIDDEN

        url = f"{self.api}/complexities/"
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 7

    def test_complexity_delete_anonymous(self):
        url = f"{self.api}/complexities/1/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

        url = f"{self.api}/complexities/"
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 7

    def test_complexity_str_representation(self):
        value = str(Complexity.objects.get(pk=1))
        assert value == "O(1)"
