from rest_framework import serializers

from .models import Comment


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = (
            "id",
            "site_user",
            "snippet",
            "title",
            "content",
            "x_position",
            "y_position",
            "up_votes",
            "down_votes",
            "rating",
        )

    def create(self, validated_data):
        validated_data["rating"] = (
            validated_data["up_votes"] - validated_data["down_votes"]
        )
        return super(CommentSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data["rating"] = (
            validated_data["up_votes"] - validated_data["down_votes"]
        )
        return super(CommentSerializer, self).update(instance, validated_data)
