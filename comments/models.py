from django.contrib.auth import get_user_model
from django.db import models

from snippets.models import Snippet


class Comment(models.Model):
    site_user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    snippet = models.ForeignKey(Snippet, on_delete=models.CASCADE, default=None)
    title = models.CharField(max_length=150, blank=False, null=False)
    content = models.TextField(blank=False, null=False)
    x_position = models.IntegerField(null=True)
    y_position = models.IntegerField(null=True)
    up_votes = models.IntegerField(default=0)
    down_votes = models.IntegerField(default=0)
    rating = models.IntegerField(default=0, db_index=True)

    class Meta:
        db_table = "comment"

    def __str__(self):
        return self.title
