import pytest
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from comments.models import Comment


@pytest.mark.django_db
class TestComments(APITestCase):
    super_username = "picard"
    super_email = "picard@example.com"
    super_password = "makeitso"
    api = "/api/v1"

    def setUp(self):
        user_model = get_user_model()
        self.superuser = user_model.objects.create_superuser(
            username=self.super_username,
            email=self.super_email,
            password=self.super_password,
        )

        self.regular_user = user_model.objects.get(username="testregularuser")
        self.regular_user.set_password("abc123")
        self.regular_user.save()

    def test_comments_unauthenticated(self):
        url = f"{self.api}/comments/"
        response = self.client.get(url)
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
            or status.HTTP_403_FORBIDDEN
        )

    def test_comments(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/comments/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 1

    def test_comment_create(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/comments/"
        response = self.client.post(
            url,
            {
                "site_user": 4,
                "snippet": 4,
                "title": "Best program ever! test",
                "content": "Pretty good comment too! test",
                "up_votes": 0,
                "down_votes": 0,
                "rating": 0,
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.data) == 10

    def test_comment_create_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/comments/"
        response = self.client.post(
            url,
            {
                "site_user": 4,
                "snippet": 4,
                "title": "Best program ever! test",
                "content": "Pretty good comment too! test",
                "up_votes": 0,
                "down_votes": 0,
                "rating": 0,
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.data) == 10

    def test_comment_create_unauthenticated(self):
        url = f"{self.api}/comments/"
        response = self.client.post(
            url,
            {
                "site_user": 4,
                "snippet": 4,
                "title": "Best program ever! test",
                "content": "Pretty good comment too! test",
                "up_votes": 0,
                "down_votes": 0,
                "rating": 0,
            },
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_comments_retrieve(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/comments/1/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_comments_update(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/comments/1/"
        response = self.client.put(
            url,
            {
                "site_user": 4,
                "snippet": 4,
                "title": "Best program ever! test",
                "content": "Pretty good comment too! test",
                "up_votes": 0,
                "down_votes": 0,
                "rating": 0,
            },
        )
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_comment_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/comments/1/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/comments/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 0

    def test_comment_delete_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/comments/1/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

        url = f"{self.api}/comments/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 1

    def test_comment_str_representation(self):
        comment = Comment.objects.get(pk=1)
        assert str(comment) == "Best program ever!"
