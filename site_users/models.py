from django.contrib.auth.models import AbstractUser
from django.db import models


class SiteUser(AbstractUser):
    bio = models.TextField(max_length=1000, blank=True, null=False, default="")
    location = models.CharField(max_length=150, blank=True, null=False, default="")

    class Meta:
        db_table = "site_user"
