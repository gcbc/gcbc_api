from rest_framework import serializers

from .models import SiteUser


class SiteUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteUser
        fields = (
            "id",
            "username",
            "first_name",
            "last_name",
            "email",
            "bio",
            "location",
            "groups",
            "user_permissions",
            "is_active",
            "is_staff",
            "is_superuser",
            "last_login",
            "date_joined",
        )
