from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import SiteUserCreationForm, SiteUserChangeForm
from .models import SiteUser


class SiteUserAdmin(UserAdmin):
    model = SiteUser
    add_form = SiteUserCreationForm
    form = SiteUserChangeForm


admin.site.register(SiteUser, SiteUserAdmin)
