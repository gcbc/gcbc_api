import pytest
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase


@pytest.mark.django_db
class TestSiteUsers(APITestCase):
    super_username = "picard"
    super_email = "picard@example.com"
    super_password = "makeitso"
    api = "/api/v1"

    def setUp(self):
        user_model = get_user_model()
        self.superuser = user_model.objects.create_superuser(
            username=self.super_username,
            email=self.super_email,
            password=self.super_password,
        )

        self.regular_user = user_model.objects.get(username="testregularuser")
        self.regular_user.set_password("abc123")
        self.regular_user.save()

    def test_site_users_unauthenticated(self):
        url = f"{self.api}/users/"
        response = self.client.get(url)
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
            or status.HTTP_403_FORBIDDEN
        )

    def test_site_users(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/users/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 5

    def test_site_users_retrieve(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/users/4/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_site_users_update(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/users/4/"
        response = self.client.put(
            url,
            {
                "last_login": "2019-08-25T04:36:16.617Z",
                "is_superuser": False,
                "username": "testregularuser",
                "first_name": "regular",
                "last_name": "user_updated",
                "email": "regularuser@example.com",
                "is_staff": False,
                "is_active": False,
                "date_joined": "2019-08-23T00:36:57Z",
                "groups": [],
                "user_permissions": [],
            },
        )
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_site_user_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/users/4/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/users/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 4

    def test_site_user_system_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/users/1/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        url = f"{self.api}/users/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 5
