from rest_framework import viewsets, mixins
from rest_framework.exceptions import ValidationError

from gcbc_api.permissions import IsOwnerOrStaffOrReadOnly

from .models import SiteUser
from .serializers import SiteUserSerializer


class SiteUserViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    queryset = SiteUser.objects.all()
    serializer_class = SiteUserSerializer

    permission_classes = [IsOwnerOrStaffOrReadOnly]

    def perform_destroy(self, instance):
        if instance.username.lower() == "system":
            raise ValidationError("You are not allowed to perform this action.")
        instance.delete()
