from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import SiteUser


class SiteUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = SiteUser
        fields = ("username", "email")


class SiteUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        model = SiteUser
        fields = UserChangeForm.Meta.fields
