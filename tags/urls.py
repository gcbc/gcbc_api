from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import TagViewSet

router = SimpleRouter()
router.register(r"", TagViewSet, basename="tags")

urlpatterns = [path("", include(router.urls))]
