from rest_framework import serializers

from .models import Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ("id", "text")

    def create(self, validated_data):
        text = validated_data.get("text").lower()
        tag = Tag.objects.create(text=text)
        return tag

    def update(self, instance, validated_data):
        instance.text = validated_data.get("text").lower()
        instance.save()
        return instance
