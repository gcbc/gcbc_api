from django.db import models


class Tag(models.Model):
    text = models.CharField(max_length=25, unique=True)

    class Meta:
        db_table = "tag"

    def __str__(self):
        return self.text
