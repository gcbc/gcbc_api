from pathlib import Path

import pytest

from django.core.management import call_command

from gcbc_api import settings


@pytest.fixture(scope="session")
def django_db_modify_db_settings():
    pass


@pytest.fixture(scope="session")
def django_db_setup(django_db_setup, django_db_blocker):
    settings.DATABASES["default"] = {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db_test.sqlite3",
    }
    with django_db_blocker.unblock():
        for file in Path("tests/test_data/").iterdir():
            call_command("loaddata", file)
