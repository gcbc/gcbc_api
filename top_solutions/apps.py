from django.apps import AppConfig


class TopSolutionsConfig(AppConfig):
    name = "top_solutions"
