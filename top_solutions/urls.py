from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import TopSolutionViewSet

router = SimpleRouter()
router.register(r"", TopSolutionViewSet, basename="topsolutions")

urlpatterns = [path("", include(router.urls))]
