from rest_framework import serializers

from .models import TopSolution


class TopSolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopSolution
        fields = ("id", "snippet_url", "language", "version")
