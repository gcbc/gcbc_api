from django.db import models

from languages.models import Language
from versions.models import Version


class TopSolution(models.Model):
    snippet_url = models.URLField(blank=False, null=False)
    language = models.ForeignKey(Language, on_delete=models.PROTECT)
    version = models.ForeignKey(Version, on_delete=models.PROTECT)

    class Meta:
        db_table = "topsolution"

    def __str__(self):
        return self.snippet_url
