from rest_framework import viewsets

from gcbc_api.permissions import IsAdminOrReadOnly

from .models import TopSolution
from .serializers import TopSolutionSerializer


class TopSolutionViewSet(viewsets.ModelViewSet):
    queryset = TopSolution.objects.all()
    serializer_class = TopSolutionSerializer

    permission_classes = [IsAdminOrReadOnly]
