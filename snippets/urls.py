from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import SnippetViewSet

router = SimpleRouter()
router.register(r"", SnippetViewSet, basename="snippets")

urlpatterns = [path("", include(router.urls))]
