import pytest
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from snippets.models import Snippet


@pytest.mark.django_db
class TestSnippets(APITestCase):
    super_username = "picard"
    super_email = "picard@example.com"
    super_password = "makeitso"
    api = "/api/v1"

    def setUp(self):
        user_model = get_user_model()
        self.superuser = user_model.objects.create_superuser(
            username=self.super_username,
            email=self.super_email,
            password=self.super_password,
        )

        self.regular_user = user_model.objects.get(username="testregularuser")
        self.regular_user.set_password("abc123")
        self.regular_user.save()

    def test_snippets_unauthenticated(self):
        url = f"{self.api}/snippets/"
        response = self.client.get(url)
        assert (
            response.status_code == status.HTTP_401_UNAUTHORIZED
            or status.HTTP_403_FORBIDDEN
        )

    def test_snippets(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/snippets/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 4

    def test_snippet_create(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/snippets/"
        response = self.client.post(
            url,
            {
                "title": "Add Function 2",
                "code": "def add(x, y):\r\n    return x + y",
                "description": "A simple addition program",
                "language": 1,
                "version": 1,
                "complexity": 1,
                "is_request": False,
                "up_votes": 1,
                "down_votes": 0,
                "tags": [2, 5],
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.data) == 13
        assert response.data == {
            "code": "def add(x, y):\r\n    return x + y",
            "complexity": 1,
            "description": "A simple addition program",
            "down_votes": 0,
            "id": 5,
            "is_request": False,
            "language": 1,
            "rating": 1,
            "site_user": 5,
            "tags": [2, 5],
            "title": "Add Function 2",
            "up_votes": 1,
            "version": 1,
        }

    def test_snippet_create_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/snippets/"
        response = self.client.post(
            url,
            {
                "title": "Add Function 3",
                "code": "def add(x, y):\r\n    return x + y",
                "description": "A simple addition program",
                "language": 1,
                "version": 1,
                "complexity": 1,
                "is_request": False,
                "up_votes": 0,
                "down_votes": 1,
                "tags": [2, 5],
            },
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert len(response.data) == 13
        assert response.data == {
            "code": "def add(x, y):\r\n    return x + y",
            "complexity": 1,
            "description": "A simple addition program",
            "down_votes": 1,
            "id": 5,
            "is_request": False,
            "language": 1,
            "rating": -1,
            "site_user": 4,
            "tags": [2, 5],
            "title": "Add Function 3",
            "up_votes": 0,
            "version": 1,
        }

    def test_snippet_create_unauthenticated(self):
        url = f"{self.api}/snippets/"
        response = self.client.post(
            url,
            {
                "title": "Add Function 3",
                "code": "def add(x, y):\r\n    return x + y",
                "description": "A simple addition program",
                "language": 1,
                "version": 1,
                "complexity": 1,
                "is_request": False,
                "up_votes": 0,
                "down_votes": 1,
                "tags": [2, 5],
            },
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_snippets_retrieve(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/snippets/4/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_snippets_update(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/snippets/4/"
        response = self.client.put(
            url,
            {
                "code": "def add(x, y):\r\n    return x + y",
                "complexity": 1,
                "description": "A simple addition program",
                "down_votes": 1,
                "id": 5,
                "is_request": False,
                "language": 1,
                "rating": 0,
                "site_user": 4,
                "tags": [2, 5],
                "title": "Add Function 3",
                "up_votes": 1,
                "version": 1,
            },
        )
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_snippet_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/snippets/4/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/snippets/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 3

    def test_snippet_delete_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/snippets/1/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

        url = f"{self.api}/snippets/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 4

    def test_snippet_str_representation(self):
        snippet = Snippet.objects.get(pk=1)
        assert str(snippet) == "A simple hello world program."
