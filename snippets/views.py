from rest_framework import viewsets
from rest_framework.exceptions import NotAuthenticated

from gcbc_api.permissions import IsOwnerOrStaffOrReadOnly

from .models import Snippet
from .serializers import SnippetSerializer


class SnippetViewSet(viewsets.ModelViewSet):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer

    permission_classes = [IsOwnerOrStaffOrReadOnly]

    def perform_create(self, serializer):
        if self.request.user.is_anonymous:
            raise NotAuthenticated
        serializer.save(site_user=self.request.user)
