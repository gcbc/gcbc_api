from django.contrib.auth import get_user_model
from django.db import models

from complexities.models import Complexity
from languages.models import Language
from tags.models import Tag
from versions.models import Version


class Snippet(models.Model):
    site_user = models.ForeignKey(
        get_user_model(), on_delete=models.SET_DEFAULT, default=1
    )
    title = models.CharField(max_length=150, blank=False, null=False, db_index=True)
    code = models.TextField(blank=False, null=False)
    description = models.TextField(blank=True, null=False)
    tags = models.ManyToManyField(Tag)
    language = models.ForeignKey(Language, on_delete=models.PROTECT)
    version = models.ForeignKey(Version, on_delete=models.PROTECT)
    complexity = models.ForeignKey(Complexity, on_delete=models.PROTECT)
    is_request = models.BooleanField(default=False)
    up_votes = models.IntegerField(default=0)
    down_votes = models.IntegerField(default=0)
    rating = models.IntegerField(default=0, db_index=True)

    class Meta:
        db_table = "snippet"

    def __str__(self):
        return self.description
