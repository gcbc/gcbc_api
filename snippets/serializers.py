from rest_framework import serializers

from .models import Snippet


class SnippetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Snippet
        fields = (
            "id",
            "site_user",
            "title",
            "code",
            "description",
            "tags",
            "language",
            "version",
            "complexity",
            "is_request",
            "up_votes",
            "down_votes",
            "rating",
        )

    def create(self, validated_data):
        validated_data["rating"] = (
            validated_data["up_votes"] - validated_data["down_votes"]
        )
        return super(SnippetSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data["rating"] = (
            validated_data["up_votes"] - validated_data["down_votes"]
        )
        return super(SnippetSerializer, self).update(instance, validated_data)
