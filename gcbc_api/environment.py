from os import getenv, path


# Django Settings
base_dir = path.dirname(path.dirname(path.abspath(__file__)))
debug = getenv("DJANGO_DEBUG", False)
secret_key = getenv("DJANGO_SECRET_KEY")
allowed_hosts = getenv("DJANGO_ALLOWED_HOSTS", "localhost,127.0.0.1,[::1]").split(",")

# Database Settings
database_engine = getenv("DJANGO_DATABASE_ENGINE", "django.db.backends.sqlite3")
database_name = getenv("DJANGO_DATABASE_NAME", path.join(base_dir, "db.sqlite3"))
database_host = getenv("DJANGO_DATABASE_HOST")
database_port = getenv("DJANGO_DATABASE_PORT")
try:
    database_port = int(database_port)
except TypeError:
    database_port = None
database_conn_max_age = getenv("DJANGO_DATABASE_CONN_MAX_AGE")
try:
    database_conn_max_age = int(database_conn_max_age)
except TypeError:
    database_conn_max_age = None
database_user = getenv("DJANGO_DATABASE_USER")
database_password = getenv("DJANGO_DATABASE_PASSWORD")
