from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("api/v1/administration/", admin.site.urls),
    path("api/v1/users/", include("site_users.urls")),
    path("api/v1/social/", include("social.urls")),
    path("api/v1/languages/", include("languages.urls")),
    path("api/v1/versions/", include("versions.urls")),
    path("api/v1/tags/", include("tags.urls")),
    path("api/v1/complexities/", include("complexities.urls")),
    path("api/v1/snippets/", include("snippets.urls")),
    path("api/v1/top_solutions/", include("top_solutions.urls")),
    path("api/v1/comments/", include("comments.urls")),
    path("api/v1/auth/", include("rest_framework.urls")),
]
