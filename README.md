# Good Code Bad Code API
[![pipeline status](https://gitlab.com/gcbc/gcbc_api/badges/master/pipeline.svg)](https://gitlab.com/gcbc/gcbc_api/commits/master)
[![coverage report](https://gitlab.com/gcbc/gcbc_api/badges/master/coverage.svg)](https://gitlab.com/gcbc/gcbc_api/commits/master)
This repository contains the code base for the Good Code Bad Code API.
