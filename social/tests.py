import pytest
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from social.models import Social


@pytest.mark.django_db
class TestSocial(APITestCase):
    super_username = "picard"
    super_email = "picard@example.com"
    super_password = "makeitso"

    api = "/api/v1"

    def setUp(self):
        user_model = get_user_model()
        self.superuser = user_model.objects.create_superuser(
            username=self.super_username,
            email=self.super_email,
            password=self.super_password,
        )

        self.regular_user = user_model.objects.get(username="testregularuser")
        self.regular_user.set_password("abc123")
        self.regular_user.save()

    def test_site_social_unauthenticated(self):
        url = f"{self.api}/social/"
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 4

    def test_social(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/social/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 4

    def test_social_retrieve(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/social/4/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_social_post(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/social/"
        response = self.client.post(
            url, {"site_user": 5, "provider": "email", "value": "captain@example.com"}
        )
        self.client.logout()
        assert response.status_code == status.HTTP_201_CREATED

    def test_social_update(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/social/3/"
        response = self.client.put(
            url, {"site_user": 3, "provider": "reddit", "value": "u/anothertestuser"}
        )
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK

    def test_social_delete(self):
        self.client.login(username=self.super_username, password=self.super_password)
        url = f"{self.api}/social/4/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/social/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 3

    def test_social_delete_regular_user(self):
        self.client.login(username=self.regular_user.username, password="abc123")
        url = f"{self.api}/social/4/"
        response = self.client.delete(url)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        url = f"{self.api}/social/"
        response = self.client.get(url)
        self.client.logout()
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 3

    def test_model_str_representation(self):
        obj = Social.objects.create(
            site_user=self.superuser, provider="reddit", value="u/test"
        )
        assert str(obj) == "picard - reddit"
