from rest_framework import viewsets

from gcbc_api.permissions import IsOwnerOrStaffOrReadOnly

from .models import Social
from .serializers import SocialSerializer


class SocialViewSet(viewsets.ModelViewSet):
    queryset = Social.objects.all()
    serializer_class = SocialSerializer
    permission_classes = [IsOwnerOrStaffOrReadOnly]
