from django.db import models

from site_users.models import SiteUser

PROVIDERS = (
    ("email", "Email"),
    ("website", "Website"),
    ("irc", "IRC"),
    ("github", "GitHub"),
    ("gitlab", "GitLab"),
    ("bitbucket", "BitBucket"),
    ("stack_exchange", "Stack Exchange"),
    ("linkedin", "LinkedIn"),
    ("facebook", "Facebook"),
    ("twitter", "Twitter"),
    ("discord", "Discord"),
    ("reddit", "Reddit"),
    ("steam", "Steam"),
    ("line", "Line"),
    ("paypal", "PayPal"),
    ("patreon", "Patreon"),
)


class Social(models.Model):
    site_user = models.ForeignKey(
        SiteUser, related_name="social", on_delete=models.CASCADE
    )
    provider = models.CharField(
        choices=PROVIDERS, max_length=50, blank=False, null=False
    )
    value = models.CharField(max_length=150, blank=True, null=False, default="")

    class Meta:
        db_table = "social"

    def __str__(self):
        return f"{self.site_user.username} - {self.provider}"
