from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .views import SocialViewSet

router = SimpleRouter()
router.register(r"", SocialViewSet, basename="social")

urlpatterns = [path("", include(router.urls))]
